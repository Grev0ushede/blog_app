# Ruby on Rails blog_app

This app is a blog_app to be used for the video game [*"Photo Finish"*](#).
It's an extension of Hartl's Guide. The goal is to go through the book and expand on it. I am working on this to better understand implementing different gemsets.
This will be redone once I finish learning react, where I will attempt to write this whole application without the base of Hartl's work.
