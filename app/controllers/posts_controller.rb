class PostsController < ApplicationController
  before_action :find_post, only: [:edit, :update, :show, :delete]

  #index for all posts
  def index
    @posts = Post.all
  end

  #New action for creating posts
  def new
    @post = Post.new
    if logged_in?
      flash[:alert] = "Time for another Dev Update!"
    else
      redirect_to root_url
      flash[:alert] = "Please log in to access post creation."
    end
  end

  #Create action for saving posts
  def create
    @post = Post.new(post_params)
    if @post.save
      flash[:notice] = "Your post has been created!"
      redirect_to dev_log_path
    else
      flash[:alert] = "Error creating post. Please try again."
      render :new
    end
  end

  def edit
    if logged_in?
      flash[:alert] = "Editing in progress"
    else
      redirect_to login_path
      flash[:alert] = "You need to be logged in to access editing."
    end
  end

  #update action for posts
  def update
    if @post.update_attributes(post_params)
      #flash notice and redirect
      flash[:notice] = "Post updated! Returning to Posts Index page"
      redirect_to dev_log_path(@posts)
    else
      #flash alert and redirect to edit
      flash[:alert] = "Error updating, UwU my bad."
      render :edit
    end
  end

  def show
  end

  #action for removing posts from db
  def destroy
    if Post.find(params[:id]).destroy
      #flash sucess notice and redirect
      flash[:notice] = "Post sent to gulag."
      redirect_to dev_log_path
    else
      #flash alert
      flash[:alert] = "Error deleting post!"
    end
  end

  private

    #write post parameters
    def post_params
      params.require(:post).permit(:title, :body)
    end

    #action for finding posts
    def find_post
      @post = Post.find(params[:id])
    end

end
