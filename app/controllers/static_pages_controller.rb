class StaticPagesController < ApplicationController
  def home
  end

  def about
  end

  def pre_order
  end

  def concept_art
  end

  def contact
  end
end
