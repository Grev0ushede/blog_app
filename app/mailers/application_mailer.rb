class ApplicationMailer < ActionMailer::Base
  default from: 'dog_kisser@doodlebug.com'
  layout 'mailer'
end
