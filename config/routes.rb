Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'

  resources :posts
  resources :users
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  root   'static_pages#home'
  get    '/about',       to: 'static_pages#about'
  get    '/pre_order',   to: 'static_pages#pre_order'
  get    '/concept_art', to: 'static_pages#concept_art'
  get    '/contact',     to: 'static_pages#contact'
  get    '/dev_log',     to: 'posts#index'
  get    '/new_posts',   to: 'posts#new'
  post   '/new_posts',   to: 'posts#create'
  get    '/signup',      to: 'users#new'
  get    '/login',       to: 'sessions#new'
  post   '/login',       to: 'sessions#create'
  delete '/logout',      to: 'sessions#destroy'
  get 'password_resets/new'
  get 'password_resets/edit'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
